resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-khanhp-files"
  force_destroy = true
}

resource "aws_s3_bucket_public_access_block" "app_public_files" {
  bucket = aws_s3_bucket.app_public_files.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

data "template_file" "s3_public_access_policy" {
  template = file("./templates/s3/s3-public-read-policy.json.tpl")

  vars = {
    bucket_arn = aws_s3_bucket.app_public_files.arn
  }
}

resource "aws_s3_bucket_policy" "app_public_files" {
  depends_on = [
    aws_s3_bucket_public_access_block.app_public_files
  ]

  bucket = aws_s3_bucket.app_public_files.id
  policy = data.template_file.s3_public_access_policy.rendered
}

resource "aws_s3_bucket_cors_configuration" "app_public_files" {
  bucket = aws_s3_bucket.app_public_files.bucket

  cors_rule {
    allowed_headers = ["Authorization", "Content-Length"]
    allowed_methods = ["GET"]
    allowed_origins = ["*"]
    expose_headers  = [""]
    max_age_seconds = 3000
  }
}