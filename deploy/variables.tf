variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}


variable "contact" {
  default = "khanhpham.100398@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres intance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "493305672751.dkr.ecr.ap-southeast-1.amazonaws.com/recipe-app-api-devops"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "493305672751.dkr.ecr.ap-southeast-1.amazonaws.com/recipe-app-api-proxy"
}

variable "django_secret_key" {
  description = "Secret key for Djano app"
}