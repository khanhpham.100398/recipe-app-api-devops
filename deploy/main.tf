terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-tfstate-khanhp"
    key            = "recipe-app.tfstate"
    region         = "ap-southeast-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tfstate-lock"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.1.0"
    }

  }
}

provider "aws" {
  region = "ap-southeast-1"

}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBY   = "Terraform"
  }
}

data "aws_region" "current" {}